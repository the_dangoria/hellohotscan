#!groovy

pipeline {
	agent { label 'master'}

	environment {
	    VERSION = readMavenPom().getVersion()
	    APP_NAME = "hellohotscan"
	
	    DOCKER_REGISTRY = "131.131.131.201:5000"
	    DOCKER_IMAGE_NAME = "${DOCKER_REGISTRY}/${APP_NAME}:${VERSION}"
	
	    GIT_URL = "https://gitlab.com/the_dangoria/hellohotscan.git"
	}

	stages {
		stage('Clone Code') {
			steps {
				git "${GIT_URL}"
			}
		}

		stage('Maven Build') {
			steps {
				sh "/usr/bin/mvn clean package"
			}
		}

		stage('Build Image') {
			steps {
				sh "docker build -t ${DOCKER_IMAGE_NAME} -f Dockerfile ."
			}
		}
		
		stage('push Image') {
			steps {
				sh "docker push ${DOCKER_IMAGE_NAME}"
				sh "docker rmi ${DOCKER_IMAGE_NAME}"
			}
		}

		stage('Send to testing server') {
			agent { label 'K8s-Master'}
			options { skipDefaultCheckout() }
			steps {
				sh "sudo helm upgrade --install --recreate-pods ${APP_NAME} /home/ansible/hellohotscan"
			}
		}
	}

	post {
	    always {
			junit '**/target/surefire-reports/TEST-*.xml'
			archive 'target/*.jar'
	    }
	}
}