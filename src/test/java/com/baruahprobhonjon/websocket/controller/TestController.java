package com.baruahprobhonjon.websocket.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestController {

	@LocalServerPort private int localServerPort;
	
	@Autowired private TestRestTemplate testRestTemplate;

	private String createUrl() {
		return "http://localhost:" + localServerPort + "/v1";
	}
	
	@Test
	public void test() {
		ResponseEntity<String> response = testRestTemplate.exchange(createUrl(), HttpMethod.GET, null, String.class);
		
		assertEquals("<h1 style='color: red'>Hello Hotscan!</h1>", response.getBody());
	}
}
