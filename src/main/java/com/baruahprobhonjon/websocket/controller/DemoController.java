package com.baruahprobhonjon.websocket.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1")
public class DemoController {
	
	@GetMapping
	public String testController() {
		return "<h1 style='color: red'>Hello Hotscan!</h1>";
	}
}
