FROM java:8-jre-alpine

ADD target/WebSocket-1.0.0.jar /WebSocket-1.0.0.jar

CMD java -jar WebSocket-1.0.0.jar